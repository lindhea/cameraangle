function D = fov(h, A, i)
    d1 = h*tan(A/2 - i);
    d2 = h*tan(A/2 + i);
    D = [d1, d2];
end
