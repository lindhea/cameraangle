# README #

A script to calculate necessary camera angle, given a required field of view and focal length (and vice versa).

### Usage ###

* Edit the variables to fit your needs

### Contribution guidelines ###

* Write human readable variable names. Give them aliases if you want to shorten them.
* Write a lot of comments
* Please use current example values

### Contact

Andreas Lindhé (lindhea@student.chalmers.se) created this script, contact him if something is not clear.
