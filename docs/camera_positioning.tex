\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[english]{babel}
\usepackage[iso,english]{isodate}

\usepackage{graphicx}
\usepackage[hidelinks]{hyperref} % Usage: \url{}
\usepackage{mathtools}
\usepackage[section]{placeins}

\usepackage{tabu}
\renewcommand{\arraystretch}{1.2}

\usepackage{gensymb}
\usepackage{todonotes}
\usepackage{siunitx}
\usepackage{minted}

\title{\texttt{cameraAngle.m}\\
    {\large A script for positioning ceiling mounted cameras}}
% \date{}
\author{Andreas Lindhé\\
    \href{mailto:lindhea@student.chalmers.se}{\texttt{lindhea@student.chalmers.se}}\\
    \url{https://bitbucket.org/lindhea/cameraangle.git}}

\begin{document}

\maketitle

\abstract

Calculating what field of view a camera has can be tedious. Especially if the
camera is tilted. This is the documentation for a script that makes this task
much easier. This documentation has a focus on the mathematics behind it, and
not very much on how to use the script. The script is well commented. Combined
with the README in this repo, it should be quite clear how to use it.

\begin{table}[ht]
    \centering
    \begin{tabu}{>{\itshape}X[r]X[2]}
        \texttt{f} & Focal length.\\
        \texttt{h} & Vertical distance from horizontal viewing plane to camera
                    lens.\\
        \texttt{i} & Inclination.\\
        \texttt{sensorWidth, sensorHeight} & The physical sensor size (in
        \si{\milli\metre}).\\
        \texttt{$A_h$, $A_v$} & The angle of view in horizontal and vertical
        direction respectively, as seen from the camera view finder.\\
        \hline
        AOV & Angle of view.\\
        FOV & Field of view.\\
        Inclination/camera angle    & The smallest angle between the vertical
                                    and the center line of the camera view.
    \end{tabu}
    \caption{Variables, abbreviations and terms}
    \label{tab:words}
\end{table}

\section{Camera parameters and angle}

For this script to work properly, a few camera parameters needs to be set before
the FOV can be calculated. The variable \texttt{f} is the focal length, which
simply is the focal length of the camera lens (in \si{\milli\metre}). The
physical sensor size (also in \si{\milli\metre}) is defined as
\texttt{sensorWidth} and \texttt{sensorHeight}. These numbers should be found in
the camera documentation.

With these camera parameters, the camera view angle for each direction can be
calculated by equation~\ref{eq:A}. The angle of view, $A$, in the direction
measured depends on the sensor size in that direction, $d$.

\begin{equation}
    \alpha = A = 2\cdot \arctan\left(\frac{d}{2\cdot f}\right)
    \label{eq:A}
\end{equation}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./figures/aov.pdf}
    \caption{FOV of the camera when tilted in one direction.}
    \label{fig:aov}
    By Moxfyre at English Wikipedia, CC BY-SA 3.0,
    \url{https://commons.wikimedia.org/w/index.php?curid=6545057}\\
\end{figure}

The height, \texttt{h}, is the vertical distance from the horizontal plane of
the subject (often the ground plane) to the outwards facing center point of the
lens. With no inclination, this will project a rectangular shape (with similar
proportions to the sensor) onto the horizontal plane which spans $2\cdot h\cdot
tan(\frac{A_h}{2})$ long and $2\cdot h\cdot tan(\frac{A_v}{2})$ wide.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{./figures/camera_triangle.pdf}
    \caption{FOV of the camera when tilted in one direction.}
    \label{fig:camera_triangle}
\end{figure}


\section{Tilting}

To calculate the FOV when tilting the camera, things are not quite as straight
forward. The viewing distance, however, is quite easy.

\subsection{View distance}

If we first consider the view distance on the horizontal plane, $d_2$ in
figure~\ref{fig:camera_triangle}, we can tell the top angle is $\frac{A}{2}+i$,
where $i$ is the inclination. Thus the distance

$$d_2 = h\cdot\arctan(\frac{A}{2}+i)$$

Similar for the opposite end of the field of view, the distance

$$d_1 = h\cdot\arctan(\frac{A}{2}-i)$$

The total view distance is $d_1+d_2$. To make scripting easier this calculation
is made by the function \texttt{fov} in \texttt{fov.m}.

\subsection{View width}

The width of the area on the horizontal viewing plane, covered by the field of
view, is a linear variation over distance (as is often the case with trapezia).
The shortest edge, let's call it $w_1$, is at the furthest away tip of $d_1$.
The width of it could be seen as the base of a triangle like
figure~\ref{fig:c_triangle}.

\begin{figure}[ht] \label{fig:c_triangle}
    \centering
    \includegraphics[width=0.5\textwidth]{./figures/c_triangle.pdf}
    \caption{The height $c$ (where $c$ is either $c_1$ or $c_2$; the side of the
        triangle that goes from the furthest most tip of $d_1$ (or $d_2$) to the
        top of the camera lens) with the top corner angle equal to the AOV in
        the opposite direction, $A'$.}
\end{figure}

Trivially (but not obviously), $$c = \frac{h}{\sin\left(\frac{\pi}{2} -
        (\frac{A}{2} - i)\right)}$$, and thus $w = 2\cdot c\cdot
\tan\left(\frac{A'}{2}\right)$.

These steps are done in \texttt{perspectiveWithIncline.m}, leaving the script to
calculate $w_1$ and $w_2$ and printing the dimensions of the trapezium to the
user.

\section{Camera positioning}

One of the parts that is really hard to solve by hand is to calculate the angle,
given a certain desired view distance. The equation for the view distance $d$
given the inclination $i$ (assuming reasonable AOV and an inclination less than
or equal to that) looks like $$d(i) = h\cdot\left(\tan\left(\frac{A}{2}+i\right)
    + \tan\left(\frac{A}{2}-i\right)\right)$$, and we want to solve for $i$,
given $d$.

Luckily, MATLAB solves this numerically for us. In the code, this is implemented
with \texttt{fzero}:

\begin{minted}{matlab}
guess_h = 0.2;
horizInclination = @(i_h) h*(tan(A_h/2 + i_h)+tan(A_h/2 - i_h)) ...
-horizontalDistance;
i_h = fzero(horizInclination, guess_h);
\end{minted}

Of course, MATLAB's algorithm needs a bit of heuristics to give a reasonable
result, so we need to give a guess. In most cases, 0.2 rad is a good enough
guess.

This last part is really easy, but for convenience the script also prints what
position the camera should have, relative to the closest wall (trivially equal
to $d_1$).

\begin{figure}[ht]
    \centering
    \includegraphics[width=1\columnwidth]{./figures/fov_placement.pdf}
    \caption{The FOV and camera placement, as calculated by the script.}
    \label{fig:fov_placement}
\end{figure}

The script outputs these measurements, and it's up to the user to pick and
choose the relevant numbers, and to piece these together for the real world
camera positioning. Autonomous drones mounting the cameras for you comes in the
next release.


\newpage
\section{Example output}

\begin{minted}{matlab}
--------------------------------
###  Camera position script  ###
--------------------------------

View area without inclination is: 3699*2959 mm (horizontal, vertical)

------------------------
#  View area inclined  #
------------------------

----------------
~~ Horizontal ~~
----------------

View area with 0.349 rad horizontal inclination is a isosceles trapezium with
height 4485 mm and the top and bottom edge 3067 and 5191 mm respectively.

--------------
~~ Vertical ~~
--------------

View area with 0.349 rad vertical inclination is a isosceles trapezium with
height 3499 mm and the top and bottom edge 3750 and 5693 mm respectively.

----------------------
#  Camera position   #
----------------------

----------------
~~ Horizontal ~~
----------------

To have 4100 mm horizontal FOV the camera needs a horizontal incline of 14.8 deg

This renders a trapezium with edges 3158 and 4608 mm.

The camera should be placed in the horizontal center, 976 mm from the wall.

The longest rectangle you can have is 1226 mm long

--------------
~~ Vertical ~~
--------------

To have 3500 mm vertical FOV the camera needs a vertical incline of 20.0 deg

This renders a trapezium with edges 3750 and 5695 mm.

The camera should be placed in the horizontal center, 436 mm from the wall.

The widest rectangle you can have is 2870 mm wide
\end{minted}

\end{document}
