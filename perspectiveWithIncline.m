function P = perspectiveWithIncline(h, A_v, A_h, i_v)
    % Beware! Dragons ahead! The naming convention from earlier continues here,
    % to make it consistent with vertical incline. But of course one could call
    % this for horizontal incline instead. But if you do, A_v, A_h and i_v will
    % be swapped and not make sense.

    % When dividing the triangle that is the vertical field of view and the
    % height, c is the hypotenuse. 1 is the closest one, 2 tends towards
    % infinity.
    i_v = abs(i_v);
    c1 = h/sin(pi/2 - (A_v/2 - i_v));
    c2 = h/sin(pi/2 - (A_v/2 + i_v));

    % Edges on the track:
    w1 = c1*tan(A_h/2);
    w2 = c2*tan(A_h/2);

    P = [w1, w2];
end
