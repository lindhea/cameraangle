clear; clc;
%% Calculation camera's angle of view

fprintf('--------------------------------\n');
fprintf('###  Camera position script  ###\n');
fprintf('--------------------------------\n\n');

%{
Vertial means "the up/down direction when looking through the camera
viewfinder", and horizontal is analogously the "left/right direction".

As of now this script can only handle inclination in vertical direction. It
can easily be changed to do the horizontal inclination instead.

Variables named [something]1 has to do with the closest edge (the one not
tending to infinity). Variables named [something]2 has to do with the far most
edge.
%}

%% Camera information:
% Focal lenght (mm)
f = 4.8;
% Sensor size (mm)
sensorWidth = 6.784;
sensorHeight = 5.427;
% Height (mm); normal distance between camera lens and view surface
h = 2617;

horizViewAngle = 2*atan(sensorWidth/(2*f));
vertViewAngle = 2*atan(sensorHeight/(2*f));
A_h = horizViewAngle;
A_v = vertViewAngle;

% No inclination

horizViewDist = sum(fov(h, A_h, 0));
vertViewDist = sum(fov(h, A_v, 0));
fprintf('View area without inclination is: %d*%d mm (horizontal, vertical)\n\n', round(horizViewDist), round(vertViewDist));

%% Calculation of view distance with inclination

fprintf('------------------------\n');
fprintf('#  View area inclined  #\n');
fprintf('------------------------\n\n');

% Inclination (horizontal, vertical) in rad:
% Note that this script can only do one dimension at a time.
i_h = 20*(pi/180);
i_v = 20*(pi/180);
% i_v = A_v/2

%% View with inclination:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('----------------\n');
fprintf('~~ Horizontal ~~\n');
fprintf('----------------\n\n');

% Why can't I simply do this? [d1_h, d2_h] = fov(...);
d_h = fov(h, A_h, i_h);
d1_h = d_h(1);
d2_h = d_h(2);
% If i_v = 0:
horizViewDistInclined = d1_h + d2_h;

% Perspective (only horizontal angle):
w = perspectiveWithIncline(h, A_h, A_v, i_h);
w1_h = 2*w(1);
w2_h = 2*w(2);

fprintf('View area with %.3g rad horizontal inclination is a isosceles trapezium with\nheight %d mm and the top and bottom edge %d and %d mm respectively.\n\n', i_h, round(horizViewDistInclined), round(w1_h), round(w2_h));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('--------------\n');
fprintf('~~ Vertical ~~\n');
fprintf('--------------\n\n');

% Why can't I simply do this? [d1_h, d2_h] = fov(...);
d_v = fov(h, A_v, i_v);
d1_v = d_v(1);
d2_v = d_v(2);
% If i_h = 0:
vertViewDistInclined = d1_v + d2_v;

% Perspective (only vertical angle):
w = perspectiveWithIncline(h, A_v, A_h, i_v);
w1_v = 2*w(1);
w2_v = 2*w(2);

fprintf('View area with %.3g rad vertical inclination is a isosceles trapezium with\nheight %d mm and the top and bottom edge %d and %d mm respectively.\n\n', i_v, round(vertViewDistInclined), round(w1_v), round(w2_v));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('----------------------\n');
fprintf('#  Camera position   #\n');
fprintf('----------------------\n\n');

%% Calculation of camera angle

% Specify FOV (mm):
horizontalDistance = 4100;
verticalDistance = 3500;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('----------------\n');
fprintf('~~ Horizontal ~~\n');
fprintf('----------------\n\n');

guess_h = 0.2;
horizInclination = @(i_h) h*(tan(A_h/2 + i_h)+tan(A_h/2 - i_h))-horizontalDistance;
i_h = fzero(horizInclination, guess_h);

fprintf('To have %d mm horizontal FOV the camera needs a horizontal incline of %.1f deg\n\n', horizontalDistance, i_h*(180/pi));

% Perspective (only horizontal angle):
w = perspectiveWithIncline(h, A_h, A_v, i_h);
w1_h = 2*w(1);
w2_h = 2*w(2);

d_h = fov(h, A_h, i_h);

fprintf('This renders a trapezium with edges %d and %d mm.\n\n', round(w1_h), round(w2_h));
fprintf('The camera should be placed in the horizontal center, %d mm from the wall.\n\n', round(d_h(1)));

% The largest rectangle, at most horizontalDistance wide, is:
v = horizontalDistance/2;
l = verticalDistance;
dw = w(2)-w(1);
dwv = w(2)-v;
s = (dwv*l)/dw;

fprintf('The longest rectangle you can have is %d mm long\n\n', round(s));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('--------------\n');
fprintf('~~ Vertical ~~\n');
fprintf('--------------\n\n');

guess_v = 0.2;
vertInclination = @(i_v) h*(tan(A_v/2 + i_v)+tan(A_v/2 - i_v))-verticalDistance;
i_v = fzero(vertInclination, guess_v);

fprintf('To have %d mm vertical FOV the camera needs a vertical incline of %.1f deg\n\n', verticalDistance, i_v*(180/pi));

% Perspective (only vertical angle):
w = perspectiveWithIncline(h, A_v, A_h, i_v);
w1_v = 2*w(1);
w2_v = 2*w(2);

d_v = fov(h, A_v, i_v);

fprintf('This renders a trapezium with edges %d and %d mm.\n\n', round(w1_v), round(w2_v));
fprintf('The camera should be placed in the horizontal center, %d mm from the wall.\n\n', round(d_v(1)));

% The largest rectangle, at most horizontalDistance wide, is:
v = horizontalDistance/2;
l = verticalDistance;
dw = w(2)-w(1);
dwv = w(2)-v;
s = (dwv*l)/dw;

fprintf('The widest rectangle you can have is %d mm wide\n\n', round(s));

